#pragma once

#include<iostream>
#include<iomanip>
#include<cmath>
#include"vector3.hpp"

namespace ParticleSimulator{
    template<class T>
    class Matrix3{
    public:
        //constructor
        T xx, yy, zz, xy, xz, yx, yz, zx, zy;
        Matrix3() : xx(T(0)), yy(T(0)), zz(T(0)), xy(T(0)), xz(T(0)), yx(T(0)), yz(T(0)), zx(T(0)), zy(T(0)) {} 
        Matrix3(const T _xx, const T _yy, const T _zz, const T _xy, const T _xz, const T _yx, const T _yz, const T _zx, const T _zy) : xx(_xx), yy(_yy), zz(_zz), xy(_xy), xz(_xz), yx(_yx), yz(_yz), zx(_zx), zy(_zy) {} 
        Matrix3(const T s) : xx(s), yy(s), zz(s), xy(s), xz(s), yx(s), yz(s), zx(s), zy(s) {}
        Matrix3(const Matrix3 & src) : xx(src.xx), yy(src.yy), zz(src.zz), xy(src.xy), xz(src.xz), yx(src.yx), yz(src.yz), zx(src.zx), zy(src.zy) {}
        Matrix3(const Vector3<T> & a, const Vector3<T> & b) : xx(a.x * b.x), yy(a.y * b.y), zz(a.z * b.z), xy(a.x * b.y), xz(a.x * b.z), yx(a.y * b.x), yz(a.y * b.z), zx(a.z * b.x), zy(a.z * b.y) {}

        const Matrix3 & operator = (const Matrix3 & rhs) {
            xx = rhs.xx;
            yy = rhs.yy;
            zz = rhs.zz;
            xy = rhs.xy;
            xz = rhs.xz;
            yx = rhs.yx;
            yz = rhs.yz;
            zx = rhs.zx;
            zy = rhs.zy;
            return (*this);
        }
        const Matrix3 & operator = (const T s) {
            xx = yy = zz = xy = xz = yx = yz = zx = zy = s;
            return (*this);
        }

        Matrix3 operator + (const Matrix3 & rhs) const {
            return Matrix3(xx + rhs.xx, yy + rhs.yy, zz + rhs.zz, xy + rhs.xy, xz + rhs.xz, yx + rhs.yx, yz + rhs.yz, zx + rhs.zx, zy + rhs.zy);
        }
        Matrix3 operator + (const T & rhs) const {
            return Matrix3(xx + rhs, yy + rhs, zz + rhs, xy, xz, yx, yz, zx, zy);
        }
        const Matrix3 & operator += (const Matrix3 & rhs) {
            (*this) = (*this) + rhs;
            return (*this);
        }
        Matrix3 operator - (const Matrix3 & rhs) const {
            return Matrix3(xx - rhs.xx, yy - rhs.yy, zz - rhs.zz, xy - rhs.xy, xz - rhs.xz, yx - rhs.yx, yz - rhs.yz, zx - rhs.zx, zy - rhs.zy);
        }
        const Matrix3 & operator -= (const Matrix3 & rhs) {
            (*this) = (*this) - rhs;
            return (*this);
        }
        Matrix3 operator * (const T & rhs) const {
            return Matrix3(xx * rhs, yy * rhs, zz * rhs, xy * rhs, xz * rhs, yx * rhs, yz * rhs, zx * rhs, zy * rhs);
        }
        Vector3<T> operator * (const Vector3<T> & rhs) const {
            return Vector3<T>(xx * rhs.x + xy * rhs.y + xz * rhs.z, yx * rhs.x + yy * rhs.y + yz * rhs.z, zx * rhs.x + zy * rhs.y + zz * rhs.z);
        }
        Matrix3 operator / (const T & rhs) const {
            return Matrix3(xx / rhs, yy / rhs, zz / rhs, xy / rhs, xz / rhs, yx / rhs, yz / rhs, zx / rhs, zy / rhs);
        }
        const Matrix3 & operator *= (const T & rhs) {
            (*this) = (*this) * rhs;
            return (*this);
        }
        friend Matrix3 operator * (const T s, const Matrix3 & m) {
            return (m * s);
        }
        const Matrix3 operator - () const {
            return Matrix3(-xx, -yy, -zz, -xy, -xz, -yx, -yz, -zx, -zy);
        }

        const Matrix3 & operator /= (const T & rhs) {
            (*this) = (*this) / rhs;
            return (*this);
        }

        Matrix3 operator * (const Matrix3 & rhs) const {
            return Matrix3(xx * rhs.xx + xy * rhs.yx + xz * rhs.zx, yx * rhs.xy + yy * rhs.yy + yz * rhs.zy, zx * rhs.xz + zy * rhs.yz + zz * rhs.zz, xx * rhs.xy + xy * rhs.yy + xz * rhs.zy, xx * rhs.xz + xy * rhs.yz + xz * rhs.zz, yx * rhs.xx + yy * rhs.yx + yz * rhs.zx, yx * rhs.xz + yy * rhs.yz + yz * rhs.zz, zx * rhs.xx + zy * rhs.yx + zz * rhs.zx, zx * rhs.xy + zy * rhs.yy + zz * rhs.zy);
        }

        T getTrace() const {
            return (xx + yy + zz);
        }
        const T getDeterminant() const {
            return (xx * yy * zz + xy * yz * zx + xz * yx * zy - xy * yx * zz - xz * zx * yy - zz * yx * xy);
        }
        const Matrix3 getTransposed() const {
            return Matrix3(xx, yy, zz, yx, zx, xy, zy, xz, yz);
        }
        const Matrix3 getInverse() const {
            return Matrix3(yy * zz - yz * zy, xx * zz - xz * zx, xx * yy - xy * yx, -xy * zz + xz * zy, xy * yz - xz * yy, - yx * zz + yz * zx, - xx * yz + xz * yx, yx * zy - yy * zx, - xx * zy + xy * zx) / getDeterminant();
        }

        template <typename U>
        operator Matrix3<U> () const {
            return Matrix3<U>(static_cast<U>(xx), static_cast<U>(yy), static_cast<U>(zz), static_cast<U>(xy), static_cast<U>(xz), static_cast<U>(yx), static_cast<U>(yz), static_cast<U>(zx), static_cast<U>(zy));
        }

        friend std::ostream & operator << (std::ostream & c, const Matrix3 & mat){
            c<<mat.xx<<"   "<<mat.xy<<"    "<<mat.xz<<std::endl;
            c<<mat.yx<<"   "<<mat.yy<<"    "<<mat.yz<<std::endl;
            c<<mat.zx<<"   "<<mat.zy<<"    "<<mat.zz<<std::endl;
            return c;
        }
    };
}
