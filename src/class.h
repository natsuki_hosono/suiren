#pragma once



enum TYPE{
	HYDRO,
	FREEZE,
};

struct system_t{
	PS::F64 dt, time;
	PS::S64 step;
	system_t() : step(0), time(0.0), dt(0.0){
	}
};

class FileHeader{
public:
	int Nbody;
	double time;
	int readAscii(FILE* fp){
		fscanf(fp, "%lf\n", &time);
		fscanf(fp, "%d\n", &Nbody);
		return Nbody;
	}
	void writeAscii(FILE* fp) const{
		fprintf(fp, "%e\n", time);
		fprintf(fp, "%d\n", Nbody);
	}
};

namespace STD{
	namespace RESULT{
		//Density summation
		class Dens{
			public:
			PS::F64 dens;
			PS::F64 smth;
			void clear(){
				dens = smth = 0;
			}
		};
		//for Balsara switch
		class Drvt{
			public:
			PS::F64 div_v;
			PS::F64vec rot_v;
			Matrix dya_v;
			void clear(){
				div_v = 0.0;
				rot_v = 0.0;
				dya_v = 0.0;
			}
		};
		//Hydro force
		class Hydro{
			public:
			PS::F64vec acc;
			PS::F64 dt;
			void clear(){
				acc = 0.0;
				dt = 1.0e+30;
			}
		};
	}

	class RealPtcl{
		public:
		PS::F64 mass;
		PS::F64vec pos, vel, acc;
		PS::F64 dens;//DENSity
		PS::F64 pres;//PRESsure
		PS::F64 smth;//SMooTHing length
		PS::F64 snds; //SouND Speed
		PS::F64 div_v;
		PS::F64vec rot_v;
		Matrix dya_v;

		Matrix srt;//stran rate tensor
		Matrix rrt;//rotation rate tensor
		Matrix dst;//deviatoric stress tensor
		PS::F64 Bal; //Balsala switch
		PS::F64 AVa; //Time dependent AV_alpha

		PS::F64vec vel_half;
		PS::F64 dt;

		PS::S64 id, tag;

		const EoS::EoS_t<PS::F64>* EoS;
		const Viscosity::Viscosity_t<PS::F64>* visc;

		TYPE type;
		//Constructor
		RealPtcl(){
			type = HYDRO;
			AVa = 1.0;
			Bal = 1.0;
		}
		//Copy functions
		void copyFromForce(const RESULT::Dens& dens){
			this->dens = dens.dens;
			this->smth = dens.smth;
		}
		void copyFromForce(const RESULT::Drvt& drvt){
			this->div_v = drvt.div_v;
			this->rot_v = drvt.rot_v;
			this->dya_v = drvt.dya_v;
			this->srt = 0.5 * (this->dya_v + this->dya_v.getTransposed());
			this->rrt = 0.5 * (this->dya_v - this->dya_v.getTransposed());
			if(PARAM::FLAG_B95 == true){
				this->Bal = std::abs(drvt.div_v) / (std::abs(drvt.div_v) + sqrt(drvt.rot_v * drvt.rot_v) + 1.0e-4 * this->snds / this->smth); //Balsala switch
			}else{
				this->Bal = 1.0;
			}
		}
		void copyFromForce(const RESULT::Hydro& force){
			this->acc     = force.acc;
			this->dt      = force.dt;
		}
		//Give necessary values to FDPS
		PS::F64 getCharge() const{
			return this->mass;
		}
		PS::F64vec getPos() const{
			return this->pos;
		}
		PS::F64 getRSearch() const{
			return kernel_t::supportRadius() * this->smth;
		}
		void setPos(const PS::F64vec& pos){
			this->pos = pos;
		}
		void writeAscii(FILE* fp) const{
			#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
			fprintf(fp, "%ld\t%ld\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",  id,  tag,  mass,  pos.x,  pos.y,  0.0  ,  vel.x,  vel.y,  0.0  ,  dens,  0.0,  pres);
			#else
			fprintf(fp, "%ld\t%ld\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",  id,  tag,  mass,  pos.x,  pos.y,  pos.z,  vel.x,  vel.y,  vel.z,  dens,  0.0,  pres);
			#endif
		}
		void readAscii(FILE* fp){
			#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
			double dummy1, dummy2;
			fscanf (fp, "%ld\t%ld\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", &id, &tag, &mass, &pos.x, &pos.y, NULL  , &vel.x, &vel.y, NULL  , &dens, NULL, &pres);
			#else
			fscanf (fp, "%ld\t%ld\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", &id, &tag, &mass, &pos.x, &pos.y, &pos.z, &vel.x, &vel.y, &vel.z, &dens, NULL, &pres);
			#endif
		}
		void setPressure(const EoS::EoS_t<PS::F64>* const _EoS){
			EoS = _EoS;
		}
		void initialize(){
			smth = PARAM::SMTH * pow(mass / dens, 1.0/(PS::F64)(PARAM::Dim));
			dst = 0.0;
		}
		void initialKick(const PS::F64 dt_glb){
			if(type == FREEZE) return;
			vel_half = vel + 0.5 * dt_glb * acc;
		}
		void fullDrift(const PS::F64 dt_glb){
			if(type == FREEZE) return;
			dens += - dens * dt_glb * div_v;
			dens = std::max(dens, 5.0);
			dst += dt_glb * (2.0 * EoS->ShearModulus() * (srt - 1.0/3.0 * div_v) + dst * rrt - rrt * dst);
			//von Mises criterion;
			const PS::F64 J2 = sqrt(0.5 * (srt.getTransposed() * srt).getTrace());
			dst *= std::min(1.0, 1.0/3.0 * EoS->YieldStress() * EoS->YieldStress() / J2);
			smth = PARAM::SMTH * pow(mass / dens, 1.0/(PS::F64)(PARAM::Dim));
			if(PARAM::FLAG_R00 == true) AVa += ((2.0 - AVa) * std::max(- div_v, 0.0) - (AVa - 0.1) / (smth / snds)) * dt_glb;
			pos += dt_glb * vel_half;
		}
		void predict(const PS::F64 dt_glb){
			if(type == FREEZE) return;
			vel += dt_glb * acc;
		}
		void finalKick(const PS::F64 dt_glb){
			if(type == FREEZE) return;
			vel = vel_half + 0.5 * dt_glb * acc;
		}
	};

	namespace EPI{
		class Dens{
			public:
			PS::F64vec pos;
			PS::F64    mass;
			PS::F64    smth;
			PS::S64    id;
			void copyFromFP(const RealPtcl& rp){
				this->pos  = rp.pos;
				this->mass = rp.mass;
				this->smth = rp.smth;
				this->id   = rp.id;
			}
			PS::F64vec getPos() const{
				return this->pos;
			}
			PS::F64 getRSearch() const{
				return kernel_t::supportRadius() * this->smth;
			}
			void setPos(const PS::F64vec& pos){
				this->pos = pos;
			}
		};

		class Drvt{
			public:
			PS::F64vec pos;
			PS::F64vec vel;
			PS::F64    smth;
			PS::F64    dens;
			void copyFromFP(const RealPtcl& rp){
				pos  = rp.pos;
				vel  = rp.vel;
				dens = rp.dens;
				smth = rp.smth;
			}
			PS::F64vec getPos() const{
				return this->pos;
			}
			PS::F64 getRSearch() const{
				return kernel_t::supportRadius() * this->smth;
			}
		};

		class Hydro{
			public:
			PS::F64vec pos;
			PS::F64vec vel;
			PS::F64    smth;
			PS::F64    dens;
			PS::F64    pres;
			Matrix dst;//deviatoric stress tensor
			Matrix vst;//viscous stress tensor
			PS::F64    snds;
			PS::F64    visc;
			PS::F64    Bal;
			PS::F64    AVa;
			PS::S64    id;///DEBUG
			void copyFromFP(const RealPtcl& rp){
				this->pos  = rp.pos;
				this->vel  = rp.vel;
				this->smth = rp.smth;
				this->dens = rp.dens;
				this->pres = rp.pres;
				this->dst  = rp.dst;
				this->visc = rp.visc->KineticViscosity(rp.dens, 0.0, rp.pres, rp.srt);
				this->vst  = 2.0 * rp.visc->KineticViscosity(rp.dens, 0.0, rp.pres, rp.srt) * rp.dens * (rp.srt - 1.0/3. * rp.div_v);
				this->snds = rp.snds;
				this->Bal  = rp.Bal;
				this->AVa  = rp.AVa;
				this->id   = rp.id;///DEBUG
			}
			PS::F64vec getPos() const{
				return this->pos;
			}
			PS::F64 getRSearch() const{
				return kernel_t::supportRadius() * this->smth;
			}
		};
	}

	namespace EPJ{
		class Dens{
		public:
			PS::F64    mass;
			PS::F64vec pos;
			PS::F64    smth;
			void copyFromFP(const RealPtcl& rp){
				this->mass = rp.mass;
				this->pos  = rp.pos;
				this->smth = rp.smth;
			}
			PS::F64vec getPos() const{
				return this->pos;
			}
			void setPos(const PS::F64vec& pos){
				this->pos = pos;
			}
			PS::F64 getRSearch() const{
				return kernel_t::supportRadius() * this->smth;
			}
		};
		class Drvt{
			public:
			PS::F64    mass;
			PS::F64vec pos;
			PS::F64vec vel;
			PS::F64    dens;
			PS::F64    smth;
			void copyFromFP(const RealPtcl& rp){
				this->mass = rp.mass;
				this->pos  = rp.pos;
				this->vel  = rp.vel;
				this->dens = rp.dens;
				this->smth = rp.smth;
			}
			PS::F64vec getPos() const{
				return this->pos;
			}
			void setPos(const PS::F64vec& pos){
				this->pos = pos;
			}
			PS::F64 getRSearch() const{
				return kernel_t::supportRadius() * this->smth;
			}
		};

		class Hydro{
			public:
			PS::F64vec pos;
			PS::F64vec vel;
			PS::F64    dens;
			PS::F64    mass;
			PS::F64    smth;
			PS::F64    pres;
			Matrix dst;//deviatoric stress tensor
			Matrix vst;//viscous stress tensor
			PS::F64    snds;
			PS::F64    Bal;
			PS::F64    AVa;
			TYPE type;
			PS::S64    id;///DEBUG
			void copyFromFP(const RealPtcl& rp){
				this->pos  = rp.pos;
				this->vel  = rp.vel;
				this->dens = rp.dens;
				this->pres = rp.pres;
				this->dst  = rp.dst;
				this->vst  = 2.0 * rp.visc->KineticViscosity(rp.dens, 0.0, rp.pres, rp.srt) * rp.dens * (rp.srt - 1.0/3. * rp.div_v);
				this->smth = rp.smth;
				this->mass = rp.mass;
				this->snds = rp.snds;
				this->Bal  = rp.Bal;
				this->AVa  = rp.AVa;
				this->type = rp.type;
				this->id   = rp.id;
			}
			PS::F64vec getPos() const{
				return this->pos;
			}
			PS::F64 getRSearch() const{
				return kernel_t::supportRadius() * this->smth;
			}
			void setPos(const PS::F64vec& pos){
				this->pos = pos;
			}
		};
	}
}


template <class Ptcl> class Problem{
	Problem(){
	}
	public:
	static void setupIC(PS::ParticleSystem<Ptcl>&, system_t&, PS::DomainInfo&){
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>&, system_t&){
		//std::cout << "No Ext. Force" << std::endl;
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>&, system_t&){
	}
};

