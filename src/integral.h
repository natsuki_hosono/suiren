#pragma once
template <class ThisPtcl> PS::F64 getTimeStepGlobal(const PS::ParticleSystem<ThisPtcl>& sph_system){
	PS::F64 dt = 1.0e+30;//set VERY LARGE VALUE
	for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
		#if 1
		PS::F64 dt_tmp = 1.0e+30;
		//if(sph_system[i].type == FREEZE) continue;
		dt_tmp = std::min(sqrt(sph_system[i].smth / sqrt(sph_system[i].acc * sph_system[i].acc)), dt_tmp);
		if(sph_system[i].type != FREEZE){
			dt = std::min(dt, std::min(sph_system[i].dt, PARAM::C_CFL * dt_tmp));
		}else{
			dt = std::min(dt, PARAM::C_CFL * dt_tmp);
		}
		//const Matrix dst_dot = 2.0 * sph_system[i].EoS->ShearModulus() * (sph_system[i].srt - 1./3. * sph_system[i].div_v) + sph_system[i].dst * sph_system[i].rrt - sph_system[i].rrt * sph_system[i].dst;
		//dt = std::min(dt, sph_system[i].dst.xx / dst_dot.xx);
		#else
		if(sph_system[i].type == FREEZE) continue;
		//dt = std::min(dt, PARAM::C_CFL * sph_system[i].smth / (sph_system[i].smth * std::abs(sph_system[i].div_v) + sph_system[i].snds + 1.2 * sph_system[i].snds + 2.4 * sph_system[i].smth * std::min(sph_system[i].div_v, 0.0)));
		dt = std::min(dt, PARAM::C_CFL * sph_system[i].smth / (sph_system[i].snds + PARAM::AV_STRENGTH * (1.2 * sph_system[i].snds + 2.4 * sph_system[i].smth * std::min(sph_system[i].div_v, 0.0))));
		#endif
	}
	return PS::Comm::getMinValue(dt);
}

