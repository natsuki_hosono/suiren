#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
template <class Ptcl> class Test: public Problem<Ptcl>{
	public:
	static const double END_TIME = 30.0;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const double width = 0.1;
		const double rout = 0.4;
		const double rin = rout - width;
		const double dx = rout / 64.0;
		
		PS::S32 cnt = 0;
		for(PS::F64 x = - rout ; x <= rout ; x += dx){
			for(PS::F64 y = - rout ; y <= rout ; y += dx){
				++ cnt;
				Ptcl ith;
				ith.type = HYDRO;
				ith.tag = 0;
				const double r = sqrt(x*x + y*y);
				if(r > rout || r < rin) continue;
				ith.pos.x = x;
				ith.pos.y = y;
				ith.vel.x = 0.0;
				ith.vel.y = 0.0;
				ith.setPressure(&RubberTest);
				ith.dens = 0.1;

				ith.mass = 4.0 * M_PI / 3.0 * (rout * rout) * ith.dens * 2.5;
				ith.eng  = 0.0;
				ith.id   = cnt++;
				ptcl.push_back(ith);
			}
		}
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].mass / (PS::F64)(2 * cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(2 * numPtclLocal);
			for(PS::U32 i = 0 ; i < numPtclLocal ; ++ i){
				sph_system[i] = ptcl[i];
				sph_system[i].pos.x += rout + width;
				sph_system[i].vel.x -= 0.118 / 2.0;
				sph_system[i + numPtclLocal] = ptcl[i];
				sph_system[i + numPtclLocal].pos.x -= rout + width;
				sph_system[i + numPtclLocal].vel.x += 0.118 / 2.0;
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
	}
};
#endif
