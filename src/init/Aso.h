#pragma once

template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 3600.0;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		std::vector<Ptcl> ptcl;
		const double gridsize = 100.0;//metre
		const double eng  = 0.0;
		const double land_dens = 2750.0;//granite
		const double land_mass = land_dens * pow(gridsize, 3);

		const double water_size = 5000.0; //metre
		const double water_dens = 1000.0;//water
		const double water_mass = water_dens * pow(water_size, 3);
		
		//place land
		std::ifstream ifs("out.txt");
		do{
			Ptcl ith;
			int lu;
			ifs >> ith.pos.x;
			ifs >> ith.pos.y;
			ifs >> ith.pos.z;
			ifs >> lu;
			ith.eng  = eng;
			ith.dens = land_dens;
			ith.mass = land_mass;
			ith.type = FREEZE;
			ith.tag = 0;
			ith.setPressure(&Granite);
			ptcl.push_back(ith);
			
		}while(!ifs.eof());

		const int size = ptcl.size();
		for(int cp = 1 ; cp < 6 ; ++ cp){
			for(int i = 0 ; i < size ; ++ i){
				Ptcl copy = ptcl[i];
				copy.pos.z -= gridsize * cp;
				ptcl.push_back(copy);
			}
		}
		
		//place water
		int cnt = 0;
		for(double x = 50000.0 ; x < 50000.0 + water_size ; x += gridsize){
			for(double y = 30000.0 ; y < 30000.0 + water_size ; y += gridsize){
				for(double z = 1200.0 ; z < 1200.0 + water_size ; z += gridsize){
					Ptcl ith;
					ith.pos.x = x;
					ith.pos.y = y;
					ith.pos.z = z;
					ith.eng = 0.0;
					ith.dens = water_dens;
					ith.mass = water_mass;
					ith.type = HYDRO;
					ith.tag = 1;
					ith.setPressure(&Water);
					ptcl.push_back(ith);
					++ cnt;
				}
			}
		}

		if(PS::Comm::getRank() == 0){
			sph_system.setNumberOfParticleLocal(ptcl.size());
			for(int i = 0 ; i < ptcl.size() ; ++ i){
				ptcl[i].id = i;
				sph_system[i] = ptcl[i];
				if(sph_system[i].tag == 1) sph_system[i].mass /= (double)(cnt);
			}
		}
		std::cout << sph_system.getNumberOfParticleLocal() << std::endl;
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag == 0){
				sph_system[i].setPressure(&Granite);
			}else{
				sph_system[i].setPressure(&Water);
			}
		}
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.z -= 10.0;
			//sph_system[i].acc   -= 0.1 * sph_system[i].vel / system.dt;
		}
	}
};

