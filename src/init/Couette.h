#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 10;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const PS::F64 box_x = 1.0;
		const PS::F64 box_y = 1.0;
		int N;
		std::cin >> N;
		const PS::F64 dx = std::min(box_x, box_y) / N;
		PS::S32 i = 0;
		PS::S32 cnt = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				++ cnt;
				Ptcl ith;
				ith.type = HYDRO;
				ith.tag = 0;
				if(y < 0.1 || 0.9 < y){
					ith.tag = 1;
					ith.dens = EoS::Aluminium.ReferenceDensity();
					ith.EoS = &EoS::Aluminium;
					ith.Visc = &Viscosity::Aluminium;
					if(0.9 < y) ith.vel.x = 1.0;
				}else{
					ith.tag = 0;
					ith.dens = EoS::Water.ReferenceDensity();
					ith.EoS = &EoS::Water;
					ith.Visc = &Viscosity::Water;
				}
				ith.pos.x = x;
				if(0.9 < y && y < 0.9 + dx){
					if(i % 5 == 0) continue;
					//ith.pos.x += 0.5 * dx;
				}
				ith.pos.y = y;
				ith.mass = ith.dens * box_x * box_y;
				ith.eng  = 0.0;
				ith.id   = i++;
				ptcl.push_back(ith);
			}
		}
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y / (PS::F64)(cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_X);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0), PS::F64vec(box_x, box_y));
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
	}
};
#else
#endif

