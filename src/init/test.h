#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 5.0;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const PS::F64 box_x = 10.0;
		const PS::F64 box_y =  5.0;
		const PS::F64 dx = box_y / 64.0;
		const double size = 0.025 * box_y;
		const double wall_size = 0.025 * box_x;
		
		PS::S32 i = 0;
		PS::S32 cnt = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				++ cnt;
				Ptcl ith;
				ith.type = HYDRO;
				ith.tag = -1;
				if(x < box_x - wall_size && 8.0 < x){
					//plate
					const double X = x - 0.875 * box_x;
					//const double X = x - 0.8 * box_x;
					const double Y = y - 0.1 * box_y - size - 0.05 * box_y;
					if(sqrt(X*X + Y*Y) <= 1.1 * size){
						ith.type = HYDRO;
						ith.tag = 2;
						ith.setPressure(&Basalt);
						ith.dens = Basalt.ReferenceDensity();
						ith.vel.y = - 4.0;
						//ith.setPressure(&Aluminium);
						//ith.dens = Aluminium.ReferenceDensity();
					}else{
						if(y < 0.1 * box_y){
							ith.type = FREEZE;
							ith.tag = 1;
							ith.setPressure(&Aluminium);
							ith.dens = Aluminium.ReferenceDensity();
						}else{
							continue;
						}
					}
				}else{
					continue;
				}
				#if 1
				ith.pos.x = x;
				ith.pos.y = y;
				#else
				ith.pos.x = y;
				ith.pos.y = x;
				ith.vel.x = ith.vel.y;
				ith.vel.y = 0.0;
				#endif
				ith.mass = ith.dens * box_x * box_y;
				ith.eng  = 0.0;
				ith.id   = i++;
				ptcl.push_back(ith);
			}
		}
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y / (PS::F64)(cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_X);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0), PS::F64vec(box_x, box_y));
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			//sph_system[i].acc.y -= 10.0;
		}
	}
};

template <class Ptcl> class Dam2: public Problem<Ptcl>{
	public:
	static const double END_TIME = 10.0;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		system_t dummy;
		char filename[256];
		sprintf(filename, "result/init_%05d_%05d.bin", PS::Comm::getNumberOfProc(), PS::Comm::getRank());
		std::ifstream fin(filename, std::ios::in | std::ios::binary);
		if(!fin){
			std::cout << "cannot open restart file." << std::endl;
			exit(1);
		}
		std::cout << "open" << std::endl;
		fin.read((char*)&dummy, sizeof(system_t));
		std::vector<Ptcl> ptcl;
		while(1){
			Ptcl ith;
			fin.read((char*)&ith, sizeof(Ptcl));
			if(fin.eof() == true) break;
			if(ith.tag == 3) continue;
			ptcl.push_back(ith);
		}
		fin.close();
		sph_system.setNumberOfParticleLocal(ptcl.size());
		for(std::size_t i = 0 ; i < ptcl.size() ; ++ i){
			sph_system[i] = ptcl[i];
		}
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.y -= 10.0;
		}
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].pos.y < 0.0){
				sph_system[i] = sph_system[sph_system.getNumberOfParticleLocal() - 1];
				sph_system.setNumberOfParticleLocal(sph_system.getNumberOfParticleLocal() - 1);
				i --;
			}
		}
	}
};
#else
#endif

