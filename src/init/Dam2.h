#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 3.0;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const PS::F64 box_x = 1.0;
		const PS::F64 box_y = 0.5;
		const PS::F64 dx = box_y / 64.0;
		const double slope = 0.5;
		PS::S32 i = 0;
		PS::S32 cnt = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				++ cnt;
				Ptcl ith;
				ith.type = HYDRO;
				ith.tag = 0;
				if(x < box_x * 0.05){
					ith.type = FREEZE;
					ith.tag = 1;
				}
				if(x < box_x * 0.25){
					if(y < box_y * 0.5){
						ith.type = FREEZE;
						ith.tag = 1;
					}
				}else if(x < 0.75 * box_x){
					if(y < - slope * x + (slope * 0.25 * box_x + 0.5 * box_y) || y < 0.1 * box_y){
						ith.type = FREEZE;
						ith.tag = 1;
					}
				}else{
					if(y < 0.1 * box_y){
						ith.type = FREEZE;
						ith.tag = 1;
					}
				}
				if(ith.type == HYDRO && (x > box_x * 0.25)){
					continue;
				}
				
				ith.pos.x = x;
				ith.pos.y = y;
				ith.setPressure(&Water);
				ith.dens = Water.ReferenceDensity();
				ith.mass = ith.dens * box_x * box_y;
				ith.eng  = 0.0;
				ith.id   = i++;
				ptcl.push_back(ith);
			}
		}
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y / (PS::F64)(cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_X);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0), PS::F64vec(box_x, box_y));
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].setPressure(&Water);
		}
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.y -= 10.0;
		}
	}
};
#else
#endif

