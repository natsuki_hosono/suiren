#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 5.0;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const PS::F64 box_x = 1.0;
		const PS::F64 box_y = 1.0;
		const PS::F64 dx = box_y / 16.0;

		PS::S32 i = 0;
		PS::S32 cnt = 0;

		for(PS::F64 x = -box_x + dx ; x < box_x ; x += dx){
			for(PS::F64 y = -box_y + dx ; y < box_y ; y += dx){
				const double theta = 0.;
				++ cnt;
				Ptcl ith;
				//if(sqrt(x*x + y*y) > 1.0) continue;
				//ith.type = HYDRO;
				ith.type = FREEZE;
				ith.tag = 0;
				ith.pos.x = x * cos(theta) + y * sin(theta);
				ith.pos.y = - x * sin(theta) + y * cos(theta);

				ith.setPressure(&Basalt);
				ith.dens = Basalt.ReferenceDensity();
				ith.mass = ith.dens * box_x * box_y;
				ith.eng  = 0.0;
				ith.id   = i++;

				ptcl.push_back(ith);
			}
		}

		for(PS::F64 x = -box_x + dx ; x < box_x ; x += dx){
			for(PS::F64 y = -box_x + dx ; y < box_y ; y += dx){
				++ cnt;
				Ptcl ith;
				if(sqrt(x*x + y*y) > 1.0) continue;
				ith.type = HYDRO;
				ith.tag = 1;
				ith.pos.x = x + 2.025;
				ith.pos.y = y;
				ith.vel.x = -1.0;

				ith.setPressure(&Basalt);
				ith.dens = Basalt.ReferenceDensity();
				ith.mass = ith.dens * box_x * box_y;
				ith.eng  = 0.0;
				ith.id   = i++;

				ptcl.push_back(ith);
			}
		}

		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y / (PS::F64)(cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
	}
};
#else
#endif

