#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 2.;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const PS::F64 box_x = 1.0;
		const PS::F64 dx = box_x / 32.0;

		PS::S32 i = 0;
		PS::S32 cnt = 0;

		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_x ; y += dx){
				++ cnt;
				Ptcl ith;
				ith.type = HYDRO;
				ith.tag = 0;
				ith.pos.x = x;
				ith.pos.y = y;
				
				if(y >= 0.5 * box_x) ith.vel.y = - 100.0;
				//ith.vel.x = + 100.0 * (y - 0.5 * box_x);
				ith.setPressure(&Basalt);
				ith.dens = Basalt.ReferenceDensity();
				ith.mass = ith.dens * box_x * box_x;
				ith.eng  = 0.0;
				ith.id   = i++;

				ptcl.push_back(ith);
			}
		}

		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_x / (PS::F64)(cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;

		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.y -= 10.0;
		}
	}
};
#else
#endif

