#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION

#define N_WATER (256)

template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 6.0;//end of simulation
	static const double slope_length = 5.0;
	static const double angle = 15.0 / 180.0 * M_PI; // slope angle (Degree to radian)
	static const double wall  = 0.1; // width of wall [m]
	static const double water_y = 0.8;
	static const double depos_length = 2.0;
	static const double wall_h = 0.1; //height of the wall on the slope [m]
	static const double debris_length = 0.6;
	static const double slit = 0.02;//Note: 2cm = 0.02 m
	static const double wall_w = 0.02;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		const PS::F64 box_x = wall + depos_length + slope_length * cos(angle);
		const PS::F64 box_y = wall + slope_length * sin(angle) + water_y;
		const PS::F64 dx = box_y / N_WATER;
		const PS::F64 dx_debris = box_y / 256;

		std::vector<Ptcl> ptcl;
		std::cout << dx << std::endl;
		//exit(0);

		PS::S32 i = 0;
		PS::S32 cnt = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				++ cnt;
				Ptcl ith;
				ith.type = HYDRO;
				ith.setPressure(&Aluminium);
				ith.dens = Aluminium.ReferenceDensity();
				if(x < wall){
					//left side wall
					/*
					ith.type = FREEZE;
					ith.tag  = 0;
					ith.setPressure(&Aluminium);
					ith.dens = Aluminium.ReferenceDensity();
					*/
					continue;
				}else if(x < wall + depos_length){
					//deposition zone
					ith.type = FREEZE;
					ith.tag  = 0;
					ith.setPressure(&Aluminium);
					ith.dens = Aluminium.ReferenceDensity();
					if(y > wall){
						continue;
					}
				}else if(x < wall + depos_length + slope_length * cos(angle)){
					//slope
					ith.type = FREEZE;
					ith.tag = 0;
					ith.setPressure(&Aluminium);
					ith.dens = Aluminium.ReferenceDensity();
					const double offset = wall + depos_length;
					if(y < (x - offset) * tan(angle)){
						//dump the ptcls under the slope
						continue;
					}
					if(x > wall + depos_length + (0.5 - wall_w) * slope_length * cos(angle) && x < wall + depos_length + 0.5 * slope_length * cos(angle)){
						const double x_wallstart = wall + depos_length + (0.5 - wall_w) * slope_length * cos(angle);
						const double y_wallstart = (x_wallstart - offset) * tan(angle) + wall_h;

						const double x_wallend   = wall + depos_length + 0.5 * slope_length * cos(angle);
						const double y_wallend   = (x_wallend - offset) * tan(angle) + wall_h;
						//wall on the slope
						if(y < wall + (x - offset) * tan(angle)){
							ith.type = FREEZE;
							ith.tag  = 0;
							ith.setPressure(&Aluminium);
							ith.dens = Aluminium.ReferenceDensity();
						}else if(y > wall + (x - offset) * tan(angle) + wall_h){
							continue;
						}else if(y < - x / tan(angle) + y_wallstart + x_wallstart / tan(angle) + wall_h){
							continue;
						}else if(y > - x / tan(angle) + y_wallend   + x_wallend   / tan(angle) - wall_h){
							continue;
						}else{
							ith.type = FREEZE;
							ith.tag  = 0;
							ith.setPressure(&Aluminium);
							ith.dens = Aluminium.ReferenceDensity();
						}
					}else if(x > wall + depos_length + 0.5 * slope_length * cos(angle) && x < wall + depos_length + 0.5 * slope_length * cos(angle) + debris_length * cos(angle)){
						//debris on the slope
						if(y > wall + (x - offset) * tan(angle) + wall_h && y < wall + (x - offset) * tan(angle) + 2.0 * wall_h){
							continue;
							/*
							if(cnt % 7 != 0) continue;
							ith.type = HYDRO;
							ith.vel.y = - 0.0;
							ith.tag  = 2;
							ith.setPressure(&Basalt);
							ith.dens = Basalt.ReferenceDensity();
							*/
						}else if(y > wall + (x - offset) * tan(angle)){
							continue;
						}else{
							ith.type = FREEZE;
							ith.tag  = 0;
							ith.setPressure(&Aluminium);
							ith.dens = Aluminium.ReferenceDensity();
						}
					}else{
						if(y > wall + (x - offset) * tan(angle)){
							continue;
						}
					}
				}else{
					if(y < slope_length * sin(angle)){
						continue;
					}
					ith.type = FREEZE;
					ith.tag = 0;
					ith.setPressure(&Aluminium);
					ith.dens = Aluminium.ReferenceDensity();
				}

				ith.pos.x = x;
				ith.pos.y = y;
				ith.mass = ith.dens * box_x * box_y;
				ith.eng  = 0.0;
				ith.id   = i++;
				ptcl.push_back(ith);
			}
		}
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y / (PS::F64)(cnt);
		}
		const int num_expr = ptcl.size();
		#if 1
		int cnt_debris = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx_debris){
			for(PS::F64 y = 0 ; y < box_y ; y += dx_debris){
				const double offset = wall + depos_length;
				Ptcl ith;
				++ cnt_debris;
				if(x > wall + depos_length + 0.5 * slope_length * cos(angle) && x < wall + depos_length + 0.5 * slope_length * cos(angle) + debris_length * cos(angle)){
					//debris on the slope
					if(y > wall + (x - offset) * tan(angle) + wall_h && y < wall + (x - offset) * tan(angle) + 2.0 * wall_h){
						if(cnt_debris % 7 != 0) continue;
						ith.type = HYDRO;
						ith.vel.y = - 0.0;
						ith.tag  = 2;
						ith.setPressure(&Basalt);
						ith.dens = Basalt.ReferenceDensity();
					}else{
						continue;
					}
				}else{
					continue;
				}
				ith.pos.x = x;
				ith.pos.y = y;
				ith.mass = ith.dens * box_x * box_y;
				ith.eng  = 0.0;
				ith.id   = cnt_debris;
				ptcl.push_back(ith);
			}
		}
		for(PS::U32 i = num_expr ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y / (PS::F64)(cnt_debris);
		}
		#endif


		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.y -= 9.8;
			//sph_system[i].acc   -= 1.0e-3 * sph_system[i].vel / sph_system[i].dt;
		}
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].pos.x < 0){
				sph_system[i] = sph_system[sph_system.getNumberOfParticleLocal() - 1];
				sph_system.setNumberOfParticleLocal(sph_system.getNumberOfParticleLocal() - 1);
				i --;
			}
		}
	}
};

template <class Ptcl> class Dam2: public Problem<Ptcl>{
	public:
	static const double END_TIME = 15.0;//end of simulation
	static const double slope_length = 5.0;
	static const double angle = 15.0 / 180.0 * M_PI; // slope angle (Degree to radian)
	static const double wall  = 0.1; // width of wall [m]
	static const double water_y = 0.8;
	static const double depos_length = 2.0;
	static const double wall_h = 0.1; //height of the wall on the slope [m]
	static const double debris_length = 0.6;
	static const double slit = 0.02;//Note: 2cm = 0.02 m
	static const double wall_w = 0.02;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		system_t dummy;
		char filename[256];
		//sprintf(filename, "init/init_%05d_%05d.bin", PS::Comm::getNumberOfProc(), PS::Comm::getRank());
		sprintf(filename, "init_%05d_%05d.bin", PS::Comm::getNumberOfProc(), PS::Comm::getRank());
		std::ifstream fin(filename, std::ios::in | std::ios::binary);
		if(!fin){
			std::cout << "cannot open restart file." << std::endl;
			exit(1);
		}
		std::cout << "open" << std::endl;
		fin.read((char*)&dummy, sizeof(system_t));
		std::vector<Ptcl> ptcl;
		while(1){
			Ptcl ith;
			fin.read((char*)&ith, sizeof(Ptcl));
			if(fin.eof() == true) break;
			if(ith.tag == 0){
				ith.EoS = &Aluminium;
			}else{
				ith.EoS = &Basalt;
			}
			if(ith.tag == 2 && ith.pos.x < 4.4) continue;
			ptcl.push_back(ith);
		}
		fin.close();
		sph_system.setNumberOfParticleLocal(ptcl.size());
		for(std::size_t i = 0 ; i < ptcl.size() ; ++ i){
			sph_system[i] = ptcl[i];
		}
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		if(PS::Comm::getRank() != 0) return;
		const PS::F64 box_x = wall + depos_length + slope_length * cos(angle);
		const PS::F64 box_y = wall + slope_length * sin(angle) + water_y;
		const PS::F64 dx = box_y / N_WATER;
		const double offset = wall + depos_length;

		#if 1
		//injection volume 80 [cm] x 70 [cm]
		const PS::F64 volume = 0.8 * 0.7;
		//injection slit height (z)
		const PS::F64 slit_height = slit;
		//injection slit width (y)
		const PS::F64 slit_width = 0.2;
		//injection velocity 3 [l/s] w/ slit width 0.2 [cm]
		const PS::F64 velocity = 3.0e-3 / (slit_width * slit_height);
		//injection duration 7 [sec]
		const PS::F64 duration = 7.0;
		//converted width
		const PS::F64 width  = velocity * duration / slit_height;
		//injection mass
		const PS::F64 mass = width * slit_height * Water.ReferenceDensity();
		static const int cnt = (int)(width * slit_height / (dx * dx) + 0.5);
		static PS::F64 time = 0.0;
		static int injected_cnt = 0;
		const PS::F64 spacing = 5.0 * dx;

		if (injected_cnt > cnt || system.time >= duration) return;

		if(system.time >= time){
			std::cout << "inject rate " << injected_cnt << " -> " << cnt << std::endl;
			for(double y = wall + (box_x - offset) * tan(angle) + dx + spacing ; y <= wall + (box_x - offset) * tan(angle) + slit_height + spacing ; y += dx){
				Ptcl ith;
				ith.pos.y = y;
				ith.pos.x = box_x - (system.time - time) * velocity + spacing;
				ith.vel.x = - velocity;
				ith.type = HYDRO;
				ith.tag = 1;
				ith.setPressure(&Water);
				ith.dens = Water.ReferenceDensity();
				ith.pres = ith.EoS->Pressure(ith.dens, 0.0);
				ith.snds = ith.EoS->SoundSpeed(ith.dens, 0.0);

				ith.mass = mass / (double)(cnt);
				ith.eng  = 0.0;
				ith.id   = 0;
				sph_system.setNumberOfParticleLocal(sph_system.getNumberOfParticleLocal() + 1);
				sph_system[sph_system.getNumberOfParticleLocal() - 1] = ith;
				injected_cnt ++;
			}
			time += dx / velocity;
		}
		#endif
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.y -= 9.8;
		}
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].pos.x < 0 || sph_system[i].pos.y < 0){
				sph_system[i] = sph_system[sph_system.getNumberOfParticleLocal() - 1];
				sph_system.setNumberOfParticleLocal(sph_system.getNumberOfParticleLocal() - 1);
				i --;
			}
		}
	}
};
#else
#endif

