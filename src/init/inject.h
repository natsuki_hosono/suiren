#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 10.0;
	static const PS::F64 box_x = 1.0;
	static const PS::F64 box_y = 1.0;
	static const PS::F64 size  = 0.1;
	static const PS::F64 dx = 1.0 / 64.0;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;

		PS::S32 cnt = 0;

		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				++ cnt;
				Ptcl ith;
				if(y < size || x > box_x - size || (x < size && y < box_y - y)){
					ith.type = FREEZE;
					ith.tag = 0;
					ith.setPressure(&Basalt);
					ith.dens = Basalt.ReferenceDensity();
				}else{
					continue;
				}
				ith.pos.x = x;
				ith.pos.y = y;

				ith.mass = ith.dens * box_x * box_y;
				ith.eng  = 0.0;
				ith.id   = cnt - 1;

				ptcl.push_back(ith);
			}
		}

		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y / (PS::F64)(cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		//dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_X);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0), PS::F64vec(box_x, box_y));
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag == 0){
				sph_system[i].setPressure(&Basalt);
			}else{
				sph_system[i].setPressure(&Water);
			}
		}
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		if(PS::Comm::getRank() != 0) return;
		const int cnt = (int)(box_x * box_y / (dx * dx) + 0.5);
		static PS::F64 time = 0.0;
		const PS::F64 velocity = 1.0;
		if(system.time >= time){
			std::cout << "inject" << std::endl;
			for(double y = box_y - size ; y < box_y ; y += dx){
				Ptcl ith;
				ith.pos.x = (system.time - time) * velocity;
				ith.pos.y = y;
				ith.vel.x = velocity;
				ith.acc.y = 10.0;
				ith.type = HYDRO;
				ith.tag = 1;
				ith.setPressure(&Water);
				ith.dens = Water.ReferenceDensity();
				ith.pres = ith.EoS->Pressure(ith.dens, 0.0);
				ith.snds = ith.EoS->SoundSpeed(ith.dens, 0.0);

				ith.mass = ith.dens * box_x * box_y / (double)(cnt);
				ith.eng  = 0.0;
				ith.id   = 0;
				sph_system.setNumberOfParticleLocal(sph_system.getNumberOfParticleLocal() + 1);
				sph_system[sph_system.getNumberOfParticleLocal()] = ith;
			}
			time += dx / velocity;
		}

	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.y -= 10.0;
		}
	}
};
#else
#endif

