#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 7.0;//end of simulation
	static const double slope_length = 5.0;
	static const double angle = 15.0 / 180.0 * M_PI; // slope angle (Degree to radian)
	static const double wall  = 0.1; // width of wall [m]
	static const double water_y = 0.8;
	static const double depos_length = 2.0;
	static const double wall_h = 0.1; //height of the wall on the slope [m]
	static const double debris_length = 0.6;
	static const double slit = 0.1;//Note: 2cm = 0.02 m
	static const double wall_w = 0.02;

	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		const PS::F64 box_x = wall + depos_length + slope_length * cos(angle);
		const PS::F64 box_y = wall + slope_length * sin(angle) + water_y;
		const PS::F64 dx = box_y / 128;

		std::vector<Ptcl> ptcl;
		//exit(0);

		PS::S32 i = 0;
		PS::S32 cnt = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				++ cnt;
				Ptcl ith;
				ith.type = HYDRO;
				ith.setPressure(&Aluminium);
				ith.dens = Aluminium.ReferenceDensity();
				if(x < wall){
					//left side wall
					/*
					ith.type = FREEZE;
					ith.tag  = 0;
					ith.setPressure(&Aluminium);
					ith.dens = Aluminium.ReferenceDensity();
					*/
					continue;
				}else if(x < wall + depos_length){
					//deposition zone
					ith.type = FREEZE;
					ith.tag  = 0;
					ith.setPressure(&Aluminium);
					ith.dens = Aluminium.ReferenceDensity();
					if(y > wall){
						continue;
					}
				}else if(x < wall + depos_length + slope_length * cos(angle)){
					//slope
					ith.type = FREEZE;
					ith.tag = 0;
					ith.setPressure(&Aluminium);
					ith.dens = Aluminium.ReferenceDensity();
					const double offset = wall + depos_length;
					if(y < (x - offset) * tan(angle)){
						//dump the ptcls under the slope
						continue;
					}
					if(x > wall + depos_length + (0.5 - wall_w) * slope_length * cos(angle) && x < wall + depos_length + 0.5 * slope_length * cos(angle)){
						const double x_wallstart = wall + depos_length + (0.5 - wall_w) * slope_length * cos(angle);
						const double y_wallstart = (x_wallstart - offset) * tan(angle) + wall_h;

						const double x_wallend   = wall + depos_length + 0.5 * slope_length * cos(angle);
						const double y_wallend   = (x_wallend - offset) * tan(angle) + wall_h;
						//wall on the slope
						if(y < wall + (x - offset) * tan(angle)){
							ith.type = FREEZE;
							ith.tag  = 0;
							ith.setPressure(&Aluminium);
							ith.dens = Aluminium.ReferenceDensity();
						}else if(y > wall + (x - offset) * tan(angle) + wall_h){
							continue;
						}else if(y < - x / tan(angle) + y_wallstart + x_wallstart / tan(angle) + wall_h){
							continue;
						}else if(y > - x / tan(angle) + y_wallend   + x_wallend   / tan(angle) - wall_h){
							continue;
						}else{
							ith.type = FREEZE;
							ith.tag  = 0;
							ith.setPressure(&Aluminium);
							ith.dens = Aluminium.ReferenceDensity();
						}
					}else if(x > wall + depos_length + 0.5 * slope_length * cos(angle) && x < wall + depos_length + 0.5 * slope_length * cos(angle) + debris_length * cos(angle)){
						//debris on the slope
						if(y > wall + (x - offset) * tan(angle) + wall_h && y < wall + (x - offset) * tan(angle) + 3.0 * wall_h){
							if(cnt % 28 != 0) continue;
							ith.type = HYDRO;
							ith.vel.y = - 0.0;
							ith.tag  = 2;
							ith.setPressure(&Basalt);
							ith.dens = Basalt.ReferenceDensity();
						}else if(y > wall + (x - offset) * tan(angle)){
							continue;
						}else{
							ith.type = FREEZE;
							ith.tag  = 0;
							ith.setPressure(&Aluminium);
							ith.dens = Aluminium.ReferenceDensity();
						}
					}else{
						if(y > wall + (x - offset) * tan(angle)){
							continue;
						}
					}
				}else{
					if(y < slope_length * sin(angle)){
						continue;
					}
					ith.type = FREEZE;
					ith.tag = 0;
					ith.setPressure(&Aluminium);
					ith.dens = Aluminium.ReferenceDensity();
				}

				ith.pos.x = x;
				ith.pos.y = y;
				ith.mass = ith.dens * box_x * box_y;
				ith.eng  = 0.0;
				ith.id   = i++;
				ptcl.push_back(ith);
			}
		}
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y / (PS::F64)(cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		//dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_X);
		//dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0), PS::F64vec(box_x, box_y));
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		if(PS::Comm::getRank() != 0) return;
		const PS::F64 box_x = wall + depos_length + slope_length * cos(angle);
		const PS::F64 box_y = wall + slope_length * sin(angle) + water_y;
		const PS::F64 dx = box_y / 256;
		const double offset = wall + depos_length;

		#if 1
		//height
		const PS::F64 height = slit;
		//width
		const PS::F64 width  = 5.0;
		//injection velocity
		const PS::F64 velocity = 1.0;
		//injection mass
		const PS::F64 mass = width * height * Water.ReferenceDensity();
		static const int cnt = (int)(width * height / (dx * dx) + 0.5);
		static PS::F64 time = 0.0;
		static int injected_cnt = 0;
		const PS::F64 spacing = 3.0 * dx;

		if (injected_cnt > cnt) return;

		if(system.time >= time){
			std::cout << "inject rate " << injected_cnt << " -> " << cnt << std::endl;
			for(double y = wall + (box_x - offset) * tan(angle) + dx + spacing ; y <= wall + (box_x - offset) * tan(angle) + height + spacing ; y += dx){
				Ptcl ith;
				ith.pos.y = y;
				ith.pos.x = box_x - (system.time - time) * velocity - spacing;
				ith.vel.x = - velocity;
				ith.type = HYDRO;
				ith.tag = 1;
				ith.setPressure(&Water);
				ith.dens = Water.ReferenceDensity();
				ith.pres = ith.EoS->Pressure(ith.dens, 0.0);
				ith.snds = ith.EoS->SoundSpeed(ith.dens, 0.0);

				ith.mass = mass / (double)(cnt);
				ith.eng  = 0.0;
				ith.id   = 0;
				sph_system.setNumberOfParticleLocal(sph_system.getNumberOfParticleLocal() + 1);
				sph_system[sph_system.getNumberOfParticleLocal() - 1] = ith;
				injected_cnt ++;
			}
			time += dx / velocity;
		}
		#endif
	}
	static void setEoS(PS::ParticleSystem<Ptcl>& sph_system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].tag == 1){
				sph_system[i].setPressure(&Water);
			}else if(sph_system[i].tag == 2){
				sph_system[i].setPressure(&Basalt);
			}else{
				sph_system[i].setPressure(&Aluminium);
			}
		}
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.y -= 9.8;
			//sph_system[i].acc   -= 1.0e-3 * sph_system[i].vel / sph_system[i].dt;
		}
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].pos.x < 0){
				sph_system[i] = sph_system[sph_system.getNumberOfParticleLocal() - 1];
				sph_system.setNumberOfParticleLocal(sph_system.getNumberOfParticleLocal() - 1);
				i --;
			}
		}
	}
};
#else
#endif

