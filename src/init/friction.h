#pragma once

#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 10;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const PS::F64 box_x = 5.0;
		const PS::F64 box_y = 2.0;
		int N;
		std::cin >> N;
		const PS::F64 dx = std::min(box_x, box_y) / N;
		PS::S32 i = 0;
		PS::S32 cnt = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				++ cnt;
				Ptcl ith;
				ith.type = HYDRO;
				ith.tag = 0;
				if(y < 0.5 || x < 0.5){
					if(y < 0.25) ith.type = FREEZE;
					ith.tag = 1;
					ith.dens = EoS::Aluminium.ReferenceDensity();
					ith.visc = &Viscosity::No;
					ith.setPressure(&EoS::Aluminium);
				}else if(y < box_y && x < 1.0){
					/*
					ith.dens = EoS::Water.ReferenceDensity();
					ith.visc = &Viscosity::Water;
					ith.setPressure(&EoS::Water);
					*/
					if(rand() % 29 == 0 && 0){
						ith.tag = 3;
						ith.dens = EoS::Basalt.ReferenceDensity();
						ith.visc = &Viscosity::No;
						ith.setPressure(&EoS::Basalt);
					}else{
						ith.tag = 0;
						//ith.dens = EoS::Mad0.ReferenceDensity();
						//ith.visc = &Viscosity::Mad0;
						//ith.setPressure(&EoS::Mad0);
						ith.dens = EoS::Mad1.ReferenceDensity();
						ith.visc = &Viscosity::Mad1;
						ith.setPressure(&EoS::Mad1);
						//ith.dens = EoS::Water.ReferenceDensity();
						//ith.visc = &Viscosity::Water;
						//ith.setPressure(&EoS::Water);
					}
					
				}else{
					if(rand() % 29 == 0 && 0){
						ith.tag = 3;
						ith.dens = EoS::Basalt.ReferenceDensity();
						ith.visc = &Viscosity::No;
						ith.setPressure(&EoS::Basalt);
					}else{
						ith.tag = 2;
						ith.dens = EoS::Water.ReferenceDensity();
						ith.visc = &Viscosity::Water;
						ith.setPressure(&EoS::Water);
					}
					continue;
				}
				ith.pos.x = x;
				ith.pos.y = y;
				ith.mass = ith.dens * box_x * box_y;
				ith.id   = i++;
				ptcl.push_back(ith);
			}
		}
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y / (PS::F64)(cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_X);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0), PS::F64vec(box_x, box_y));
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.y -= 9.8;
		}
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].pos.y < 0.0){
				sph_system[i] = sph_system[sph_system.getNumberOfParticleLocal() - 1];
				sph_system.setNumberOfParticleLocal(sph_system.getNumberOfParticleLocal() - 1);
				i --;
			}
		}
		if(PS::Comm::getRank() != 0) return;
		return ;
		const PS::F64 dx = 2.0 / 64.0; //inflow particle separation
		const PS::F64 duration = 5.0; //duration
		const PS::F64 start = 0.0; //start date
		const PS::F64 size = 0.5; //size of inflow tube?
		const PS::F64 velocity = 1.0; // inflow velocity
		const PS::F64vec position = PS::F64vec(1.0, 2.5); // position of inflow

		const PS::F64 mass = duration * velocity * size * EoS::Water.ReferenceDensity();
		const PS::S64 cnt = (int)(duration * velocity * size / (dx * dx) + 0.5);
		static PS::F64 time = start;

		if(system.time >= time && system.time <= start + duration){
			for(double x = position.x - size / 2.0 ; x <= position.x + size / 2.0 ; x += dx){
				Ptcl ith;
				ith.pos.y = position.y - (system.time - time) * velocity;
				ith.pos.x = x;
				ith.vel.y = - velocity;
				ith.acc.y = - 9.8;
				ith.type = HYDRO;
				ith.tag = 2;
				ith.setPressure(&EoS::Water);
				ith.visc = &Viscosity::Water;
				ith.dens = EoS::Water.ReferenceDensity();
				ith.pres = ith.EoS->Pressure(ith.dens, 0.0);
				ith.snds = ith.EoS->SoundSpeed(ith.dens, 0.0);

				ith.mass = mass / (double)(cnt);
				ith.id   = 0;
				std::cout << "inject " << ith.dens << std::endl;
				sph_system.setNumberOfParticleLocal(sph_system.getNumberOfParticleLocal() + 1);
				sph_system[sph_system.getNumberOfParticleLocal() - 1] = ith;
			}
			time += dx / velocity;
		}
	}
};
#else
#endif

