#pragma once

//static const double SRF = 0.1;

//static const double SRF = 60./1400.; //expr
//static const double SRF = 60./3116.; //collision
//static const double SRF = 31./1400.; //Dam2
static const double SRF = 60.0/1400.0;//friction
//static const double SRF = 5.0/1400.0;//friction2
//static const double SRF = 10.0/1400.0;//

namespace Viscosity{
	//////////////////
	//abstract classes
	//////////////////
	template <typename type> class YieldCriterion_t{
		public:
		YieldCriterion_t(){
			return ;
		}
		virtual ~YieldCriterion_t(){
			return ;
		}
	};
	template <typename type> class Viscosity_t{
		public:
		Viscosity_t(){
			return ;
		}
		virtual ~Viscosity_t(){
			return ;
		}
		virtual type KineticViscosity(const type, const type, const type, const Matrix) const = 0;
	};
	//////////////////
	//concrete classes
	//////////////////
	template <typename type> class Constant : public YieldCriterion_t<type>{
		const type ys;
		public:
		Constant(const type ys_): ys(ys_){
		}
		type YieldStress() const{
			return ys;
		}
		type YieldStress(const type pres) const{
			return YieldStress();
		}
	};
	template <typename type> class MohrCoulomb : public YieldCriterion_t<type>{
		const type c; //cohesion
		const type fa; //frictional angle in radian
		public:
		MohrCoulomb(const type c_, const type fa_): c(c_), fa(fa_){
		}
		type YieldStress(const type pres) const{
			//Mohr–Coulomb yield criterion
			return c + pres * tan(fa);
		}
	};
	template <typename type> class Newtonian : public Viscosity_t<type>{
		const type nu0;
		public:
		Newtonian(const type _nu0) : nu0(_nu0){
		}
		type KineticViscosity(const type, const type, const type, const Matrix) const{
			return nu0;
		}
	};
	template <typename type, typename YC_t> class Bingham : public Viscosity_t<type>{
		const type nuB;//kinetic viscosity
		const YC_t YC;
		public:
		Bingham(const type _nuB, const YC_t _YC) : nuB(_nuB), YC(_YC){
		}
		type KineticViscosity(const type dens, const type eng, const type pres, const Matrix srt) const{
			const type mu_inf = nuB * dens;
			const type mu_0 = 1000.0 * mu_inf; //1000.0 is fudge
			const type K = mu_0 / (YC.YieldStress(std::max(pres, 0.0)) + 1.0e-5);
			const type J2 = sqrt(0.5 * (srt.getTransposed() * srt).getTrace());
			const type gamma_dot = J2;
			//Shao & Lo (2003)
			return (mu_0 + K * mu_inf * gamma_dot) / (1.0 + K * gamma_dot) / dens;
		}
	};

	static const Viscosity::Newtonian<PS::F64> Water(1.0e-6);
	static const Viscosity::Bingham<PS::F64, MohrCoulomb<PS::F64> > Mad0(0.76, MohrCoulomb<PS::F64>(0, 28.0 / 360.0 * 2.0 * M_PI));
	static const Viscosity::Bingham<PS::F64, Constant<PS::F64> > Mad1(7.7e-5, Constant<PS::F64>(750.0));
	//Dammy
	static const Viscosity::Newtonian<PS::F64> No(0.0);
}

namespace EoS{
	//////////////////
	//abstract class
	//////////////////
	template <typename type> class EoS_t{
		public:
		EoS_t(){
			return ;
		}
		virtual ~EoS_t(){
			return ;
		}
		virtual type Pressure  (const type dens, const type eng) const = 0;
		virtual type SoundSpeed(const type dens, const type eng) const = 0;
		virtual type ShearModulus(void) const{
			return 0.0;
		}
		virtual type YieldStress(void) const{
			return +1.0/0.0;
		}
		virtual type ReferenceDensity(void) const{
			return +1.0/0.0;
		}
	};
	//////////////////
	//EoSs
	//////////////////
	template <typename type> class IdealGas : public EoS_t<type>{
		const type hcr;//heat capacity ratio;
		public:
		IdealGas(const type _hcr) : hcr(_hcr){
		}
		inline type Pressure(const type dens, const type eng) const{
			return (hcr - 1.0) * dens * eng;
		}
		inline type SoundSpeed(const type dens, const type eng) const{
			return sqrt(hcr * (hcr - 1.0) * eng);
		}
		inline type HeatCapacityRatio() const{
			return hcr;
		}
	};
	template <typename type> class Murnaghan : public EoS_t<type>{
		const type gmm, rho0, B, mu0;//
		public:
		Murnaghan(const type _gmm, const type _rho0, const type _c0, const type _mu0) : gmm(_gmm), rho0(_rho0), B(_c0 * _c0 * _rho0 / _gmm), mu0(_mu0){
		}
		inline type Pressure(const type dens, const type eng) const{
			return B * (pow(dens / rho0, gmm) - 1.0);
		}
		inline type SoundSpeed(const type dens, const type eng) const{
			return sqrt(std::abs(B * gmm / rho0 * pow(dens / rho0, gmm - 1.0)));
		}
		inline type ReferenceDensity(void) const{
			return rho0;
		}
		inline type ShearModulus(void) const{
			return mu0;
		}
	};
	template <typename real> class BirchMurnaghan : public EoS_t<real>{
		const real B, rho0, mu0, Y;//nu0: kinetic visc, mu0: shear modulus
		public:
		BirchMurnaghan(const real _rho0, const real _c0, const real _mu0, const real _Y) : rho0(_rho0), B(_c0 * _c0 * _rho0), mu0(_mu0), Y(_Y){
		}
		inline real Pressure(const real dens, const real eng) const{
			//return std::max(1.5 * B * (pow(dens / rho0, 7./3.) - pow(dens / rho0, 5./3.)), 1.0e-16 * 0);
			return 1.5 * B * (pow(dens / rho0, 7./3.) - pow(dens / rho0, 5./3.));
		}
		inline real SoundSpeed(const real dens, const real eng) const{
			return sqrt(std::max(0.5 * B / rho0 * (7.0 * pow(dens / rho0, 4./3.) - 5.0 * pow(dens / rho0, 2./3.)), 1.0e-16));
			//return sqrt(std::abs(0.5 * B / rho0 * (7.0 * pow(dens / rho0, 4./3.) - 5.0 * pow(dens / rho0, 2./3.))));
		}
		inline real ShearModulus(void) const{
			return mu0;
		}
		inline real ReferenceDensity(void) const{
			return rho0;
		}
		inline real YieldStress(void) const{
			return Y;
		}
	};
	static const EoS::IdealGas<PS::F64> Monoatomic(5./3.);
	static const EoS::IdealGas<PS::F64> Diatomic  (1.4);

	static const EoS::BirchMurnaghan<PS::F64> Water    (1000.0, SRF * 1400.0,              0.0,                 0.0   );
	static const EoS::BirchMurnaghan<PS::F64> Mad0     (1650.0, SRF * 1500.0,              0.0,                 0.0   );
	static const EoS::BirchMurnaghan<PS::F64> Mad1     (1950.0, SRF * 1500.0,              0.0,                 0.0   );
	static const EoS::BirchMurnaghan<PS::F64> Basalt   (2750.0, SRF * 3116.0, SRF * SRF * 22.70e+9, SRF * SRF * 3.5e+9);
	static const EoS::BirchMurnaghan<PS::F64> Aluminium(2700.0, SRF * 5277.0, SRF * SRF * 25.50e+9, SRF * SRF * 15.0e+9);
	static const EoS::BirchMurnaghan<PS::F64> Iron     (7800.0, SRF * 4051.0, 52.50e+9, 80.0e+9);
	static const EoS::BirchMurnaghan<PS::F64> Lucite   (1180.0, SRF * 2926.0, 74.00e+6,  1.0e+7);
	static const EoS::BirchMurnaghan<PS::F64> Rubber   (1100.0, SRF *  134.8, 4.27e+6, +1.0/0.0);

	static const EoS::Murnaghan<PS::F64> RubberTest(1.0, 0.1, 1.0, 0.22);
}



