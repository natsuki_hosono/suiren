#pragma once
namespace STD{
	class CalcDensity{
		kernel_t kernel;
		public:
		void operator () (const EPI::Dens* const ep_i, const PS::S32 Nip, const EPJ::Dens* const ep_j, const PS::S32 Njp, RESULT::Dens* const dens){
			for(PS::S32 i = 0 ; i < Nip ; ++ i){
				const EPI::Dens& ith = ep_i[i];
				for(PS::S32 j = 0 ; j < Njp ; ++ j){
					const EPJ::Dens& jth = ep_j[j];
					const PS::F64vec dr = jth.pos - ith.pos;
					dens[i].dens += jth.mass * kernel.W(dr, ith.smth);
				}
				dens[i].smth = PARAM::SMTH * pow(ith.mass / dens[i].dens, 1.0/(PS::F64)(PARAM::Dim));
			}
		}
	};
	void CalcPressure(PS::ParticleSystem<STD::RealPtcl>& sph_system){
		#pragma omp parallel for
		for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].pres = sph_system[i].EoS->Pressure(sph_system[i].dens, 0.0);
			sph_system[i].snds = sph_system[i].EoS->SoundSpeed(sph_system[i].dens, 0.0);
		}
	}
	class CalcDerivative{
		kernel_t kernel;
		public:
		void operator () (const EPI::Drvt* ep_i, const PS::S32 Nip, const EPJ::Drvt* ep_j, const PS::S32 Njp, RESULT::Drvt* const drvt){
			for(PS::S32 i = 0; i < Nip ; ++ i){
				const EPI::Drvt& ith = ep_i[i];
				#if 0
				Matrix Minv = 0.0;
				for(PS::S32 j = 0; j < Njp ; ++ j){
					const EPJ::Drvt& jth = ep_j[j];
					const PS::F64vec dr = jth.pos - ith.pos;
					Minv += Matrix(dr, dr) * jth.mass / jth.dens * kernel.W(dr, ith.smth);
				}
				Minv = Minv.getInverse();
				for(PS::S32 j = 0; j < Njp ; ++ j){
					const EPJ::Drvt& jth = ep_j[j];
					const PS::F64vec dr = jth.pos - ith.pos;
					const PS::F64vec dv = jth.vel - ith.vel;
					drvt[i].div_v += dv * (Minv * dr) * jth.mass / jth.dens * kernel.W(dr, ith.smth);
					drvt[i].rot_v += dv ^ (Minv * dr) * jth.mass / jth.dens * kernel.W(dr, ith.smth);
					drvt[i].dya_v += Matrix(dv, (Minv * dr) * jth.mass / jth.dens * kernel.W(dr, ith.smth));
				}
				#else
				for(PS::S32 j = 0; j < Njp ; ++ j){
					const EPJ::Drvt& jth = ep_j[j];
					const PS::F64vec dr = ith.pos - jth.pos;
					const PS::F64vec dv = ith.vel - jth.vel;
					const PS::F64vec gradW = kernel.gradW(dr, ith.smth);
					drvt[i].div_v += - jth.mass * dv * gradW;
					drvt[i].rot_v += - jth.mass * dv ^ gradW;
					drvt[i].dya_v += - jth.mass * Matrix(dv, gradW);
				}
				drvt[i].div_v /= ith.dens;
				drvt[i].rot_v /= ith.dens;
				drvt[i].dya_v /= ith.dens;
				#endif
			}
		}
	};

	class CalcHydroForce{
		const kernel_t kernel;
		public:
		void operator () (const EPI::Hydro* const ep_i, const PS::S32 Nip, const EPJ::Hydro* const ep_j, const PS::S32 Njp, RESULT::Hydro* const hydro){
			for(PS::S32 i = 0; i < Nip ; ++ i){
				PS::F64 v_sig_max = 0.0;
				const EPI::Hydro& ith = ep_i[i];
				for(PS::S32 j = 0; j < Njp ; ++ j){
					const EPJ::Hydro& jth = ep_j[j];
					const PS::F64vec dr = ith.pos - jth.pos;
					const PS::F64vec dv = ith.vel - jth.vel;
					const PS::F64 w_ij = (dv * dr < 0) ? dv * dr / sqrt(dr * dr) : 0;
					const PS::F64 v_sig = ith.snds + jth.snds - 3.0 * w_ij;
					v_sig_max = std::max(v_sig_max, v_sig);
					PS::F64 AV = - PARAM::AV_STRENGTH * 0.5 * v_sig * w_ij / (0.5 * (ith.dens + jth.dens)) * 0.5 * (ith.Bal + jth.Bal);
					if(PARAM::FLAG_R00 == true) AV *= 0.5 * (ith.AVa + jth.AVa);
					const PS::F64vec gradW = 0.5 * (kernel.gradW(dr, ith.smth) + kernel.gradW(dr, jth.smth));
					#if 0
						//pressure gradient
						hydro[i].acc     -= jth.mass * ((- ith.dst + ith.pres) / (ith.dens * ith.dens) + (- jth.dst + jth.pres) / (jth.dens * jth.dens) + AV) * gradW;
						//viscosity
						//hydro[i].acc     += jth.mass * 4.0 * 0.5 * (ith.EoS->KineticViscosity() + jth.EoS->KineticViscosity()) * dr * gradW / (ith.dens + jth.dens) / sqrt(dr * dr + 1.0e-4 * (ith.smth + jth.smth)) * dv;
						hydro[i].acc     += jth.mass * 4.0 * 0.5 * (ith.EoS->KineticViscosity() + jth.EoS->KineticViscosity()) * dr * gradW / (ith.dens + jth.dens) / (dr * dr + 1.0e-4 * (ith.smth * jth.smth)) * dv;
					#else
						//pressure gradient
						/*
						const PS::F64 fij = 0.5 * (kernel.W(dr, ith.smth) + kernel.W(dr, jth.smth)) / (kernel.W(ith.smth / 1.2, ith.smth));
						hydro[i].acc     -= jth.mass * ((- ith.dst - ith.vst + ith.pres) / (ith.dens * ith.dens) + (- jth.dst - jth.vst + jth.pres) / (jth.dens * jth.dens) + AV + AS) * gradW;
						*/
						hydro[i].acc     -= jth.mass * ((- ith.dst - ith.vst + ith.pres) / (ith.dens * ith.dens) + (- jth.dst - jth.vst + jth.pres) / (jth.dens * jth.dens) + AV) * gradW;
					#endif
					//hydro[i].eng_dot += jth.mass * (ith.pres / (ith.dens * ith.dens) + 0.5 * AV) * dv * gradW;
				}
				const PS::F64 J2 = sqrt(0.5 * (ith.dst.getTransposed() * ith.dst).getTrace());
				//hydro[i].dt = PARAM::C_CFL * 2.0 * ith.smth / v_sig_max;
				//hydro[i].dt = PARAM::C_CFL * 2.0 * ith.smth / (v_sig_max + std::abs(ith.dst.getTrace()) / ith.dens);
				hydro[i].dt = PARAM::C_CFL * 2.0 * ith.smth / (v_sig_max + sqrt(J2 / ith.dens) + ith.visc / ith.smth);
				//hydro[i].dt = PARAM::C_CFL * 2.0 * ith.smth / (v_sig_max + sqrt(sqrt(2.0 * ith.dst.getSecondInvariant()) / ith.dens));
			}
		}
	};
}


